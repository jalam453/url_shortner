from django.contrib import admin
from django.urls import path
from urlhandler.views import dashboard, generate, get_orignal_url

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', dashboard),
    path('dashboard/', dashboard, name="dashboard"),
    path('generate/', generate, name="generate"),
    path('get_orignal_url/', get_orignal_url, name="get_orignal_url"),
]