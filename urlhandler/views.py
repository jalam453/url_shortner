from django.shortcuts import render, redirect
from django.contrib import messages
import random
import string

urls = {}

def dashboard(request):
    urls = {}
    short = ''
    orignal_url = ''

    if 'short' in request.session:
        short = request.session['short']
        request.session['short'] = ''

    if 'orignal_url' in request.session:
        orignal_url = request.session['orignal_url']
        request.session['orignal_url'] = ''

    if 'urls' in request.session:
        urls = request.session['urls']
    return render(request, 'dashboard.html', {'urls': urls, 'short_url': short, 'orignal_url': orignal_url})

def random_generator():
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(6))

def generate(request):
    global urls
    if request.method == "POST":
        request.session['urls'] = urls
        if request.POST['original']:
            original = request.POST['original']
            short = random_generator()

            if original not in request.session['urls']:
                urls[original] = short
                request.session['urls'] = urls
                request.session['short'] = short
                request.session['orignal_url'] = original
                return redirect(dashboard)
            else:
                messages.error(request, "Already Exists")
            return redirect(dashboard)
        else:
            messages.error(request, "Empty Fields")
            return redirect(dashboard)
    else:
        return redirect('/dashboard')

def get_orignal_url(request):
    orignal_url = str()
    urls = {}

    if request.method == "POST":
        if 'urls' in request.session:
            urls = request.session['urls']

        if request.POST['short_url']:
            short_url = request.POST['short_url']
            short_key = short_url.split('http://localhost:8000/')[1]
            orignal_url = list(urls.keys())[list(urls.values()).index(short_key)]
            request.session['orignal_url'] = orignal_url
    return redirect('/dashboard')
