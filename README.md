
# URL Shortner
A URL Shortening Service Coded In **Django.**
 
 
## Initial Commit

1. Random URL Generation

This URL shortner service you help you shorten your URL. <br>
This is build using Django and Python

## Steps to Run

1. Clone this project 
   <br>git clone https://gitlab.com/jalam453/url_shortner.git
   
2. Start your django server
<br> python manage.py runserver
```Make sure you have Django and python installed```
3. Run in web browser
<br> Type http://127.0.0.1:8000/ in your browser